Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  get '/calculadora', to: 'calculator#math'
  get '/calculadora/:num1/:num2', to: 'calculator#math'

  get '/calculadora/:num1/:num2/sum', to: 'calculator#sum', as: "sum"
  get '/calculadora/:num1/:num2/minus', to: 'calculator#minus', as: "minus"
  get '/calculadora/:num1/:num2/multiply', to: 'calculator#multiply', as: "multiply"
  get '/calculadora/:num1/:num2/division', to: 'calculator#division', as:"division"

end
